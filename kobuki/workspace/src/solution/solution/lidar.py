#!/usr/bin/python3
print("-> lidar.py")

import numpy as np
import cv2 as cv
from copy import deepcopy


def lidarDisplay(lidar):
    mat = np.zeros((500, 500), dtype=np.uint8)

    cy, cx = mat.shape
    cx /= 2
    cy /= 2

    for c, i in enumerate(lidar.ranges):
        if np.isinf(i):
            i = 10.0

        x = cx - np.sin(lidar.angle_min + c * lidar.angle_increment) * i * 25
        y = cy - np.cos(lidar.angle_min + c * lidar.angle_increment) * i * 25

        cv.line(mat, (int(cx), int(cy)), (int(x), int(y)), 255)
    cv.imshow("Lidar", mat)


def lidarRotate(lidar, rotation):
    out = deepcopy(lidar)
    rot = int(rotation / lidar.angle_increment)

    while rot < 0:
        rot += 300

    for c, i in enumerate(lidar.ranges):
        out.ranges[c - rot] = i

    return out


def lidarFindInfs(lidar):
    infsIndex = []
    infsLenghts = []
    infsVectors = []

    lenght = 0
    for i in range(-30, 300):
        if np.isinf(lidar.ranges[getNormilizedLidarIndex(i)]):
            lenght += 1
        else:
            if lenght > 5:
                i0 = i - lenght
                i1 = i - lenght / 2
                i2 = i
                infsIndex.append((i0, i1, i2))

                l0 = g.lidar.ranges[i0]
                l1 = g.lidar.ranges[i1]
                l2 = g.lidar.ranges[i2]
                infsLenghts.append((l0, l1, l2))

                p0 = getLidarPointTrace(i0, l0)
                if p0[0] < 0 or p0[0] > mapSize or p0[1] < 0 or p0[1] > mapSize or map_[p0[1], p0[0], 0] < 100:
                    p0 = None

                p1 = getLidarPointTrace(i1, l1)
                if p1[0] < 0 or p1[0] > mapSize or p1[1] < 0 or p1[1] > mapSize or map_[p1[1], p1[0], 0] < 100:
                    p1 = None

                p2 = getLidarPointTrace(i2, l2)
                if p2[0] < 0 or p2[0] > mapSize or p2[1] < 0 or p2[1] > mapSize or map_[p2[1], p2[0], 0] < 100:
                    p2 = None

                infsVectors.append((p0, p1, p2))
            lenght = 0
    return infsIndex, infsLenghts, infsVectors
