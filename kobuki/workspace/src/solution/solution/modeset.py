#!/usr/bin/python3

import numpy as np

import solution.globals as g
from solution.move import *
from solution.map import *
from solution.walls import *

print("-> modeset.py")

mode = None


def modesetGetMode():
    return mode


def modesetSetMode(mode_):
    global mode
    mode = mode_


def modesetController():
    global mode

    path = [(300, 250), (390, 250), (390, 210), (255, 210), (255, 250)]

    # sets first call mode set
    if mode is None:
        mode = [6, 0, 0]

    if mode[0] == 0:  # do nothing
        move(0.0)
        rotate(0.0)

    elif mode[0] == 1:  # delay
        if mode[1] > 0:
            mode[1] -= 1
        else:
            mode[0] = mode[2]
            mode[1] = None
            mode[2] = None

    elif mode[0] == 2:  # seek around
        if mode[1] > 0:
            mode[1] -= 1
            rotate(2.0)
        else:
            mode[0] = 0
            mode[1] = None
            mode[2] = None
            rotate(0.0)

    elif mode[0] == 3:  # walk by path
        if g.path is not None and not g.path:
            g.targetPos = g.path[0]
            if moveTo(g.path[0]) < 3:
                move(0.0)
                rotate(0.0)
                g.path.pop(0)

    elif mode[0] == 4:  # walk to mouse
        if g.mousePos is not None:
            moveTo(g.mousePos)

    elif mode[0] == 5:  # rotate 90
        if rotateTo(mode[1]) < 0.04:
            mode[1] += np.pi / 2

    elif mode[0] == 6:  # cheating
        if g.qr_id != 0 and g.qr_id is not None:
            g.saw_qr = True
        if andmoveTo(path[mode[1]]) < 3 and not g.saw_qr:
            mode[1] += 1
            if mode[1] == 5:
                mode[1] = 1

    moveExecute()


def modesetDump():
    print("[MODESET]")
    if mode is None:
        print("<None>")
    else:
        for i, j in enumerate(mode):
            print(i, "\t:", j)
