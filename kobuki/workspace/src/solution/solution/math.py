#!/usr/bin/python3
print("-> np.py")

import numpy as np

import solution.globals as g
from solution.map import *


def getVectorAdd1(a, b):
    return a[0] + b, a[1] + b


def getVectorAdd2(a, b):
    return a[0] + b[0], a[1] + b[1]


def getVectorSub1(a, b):
    return a[0] - b, a[1] - b


def getVectorSub2(a, b):
    return a[0] - b[0], a[1] - b[1]


def getVectorMul1(a, b):
    return a[0] * b, a[1] * b


def getVectorMul2(a, b):
    return a[0] * b[0], a[1] * b[1]


# computes distance between two points
# takes two vector
# returns distance
def getDistance2P(a, b):
    return np.sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)


# computes angle between two points
# takes two vector
# returns angle in radians
def getAngle2P(a, b):
    return np.arctan2(b[0] - a[0], b[1] - a[1])


# compute euler angles from quternion
# takes quternion angles
# returns euler angles
def getAngleFromQuaternion(w, x, y, z):
    y2 = y * y

    t0 = 2.0 * (w * x + y * z)
    t1 = 1.0 - 2.0 * (x * x + y2)
    X = np.arctan2(t0, t1)

    t2 = 2.0 * (w * y - z * x)
    t2 = np.where(t2 > 1.0, 1.0, t2)

    t2 = np.where(t2 < -1.0, -1.0, t2)
    Y = np.arcsin(t2)

    t3 = 2.0 * (w * z + x * y)
    t4 = 1.0 - 2.0 * (y2 + z * z)
    Z = np.arctan2(t3, t4)

    return X, Y, Z


# computes end position of lidar ray
# takes ray index (i)
# and ray lenght (l)
def getLidarPointTrace(i, m):
    pos = mapTranslateTo(g.pos)
    return pos[0] + np.sin(i * g.lidar.angle_increment) * m, pos[1] + np.cos(i * g.lidar.angle_increment) * m


# computes point on a circle
# takes angle in radians (r)
# and circle radius (m)
def getPointOnCircle(r, m):
    return np.sin(r) * m, np.cos(r) * m


# computes intersection between two line segments
# takes first line segment vectors (a, b)
# and second line segment vectors (c, d)
# returns True if lines intersects otherwise False
def getTwoLineSegmentsIntersection(a, b, c, d):
    def ccw(a, b, c):
        return ((c[1] - a[1]) * (b[0] - a[0])) > ((b[1] - a[1]) * (c[0] - a[0]))

    return ccw(a, c, d) != ccw(b, c, d) and ccw(a, b, c) != ccw(a, b, d)


# computes intersection point between two line segment
# takes first line segment vectors (a, b)
# and second line segment vectors (c, d)
# returns point of intersection
# or zero vector if lines are parallel
def getTwoLineIntersectionPoint(a, b, c, d):
    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    dx = (a[0] - b[0], c[0] - d[0])
    dy = (a[1] - b[1], c[1] - d[1])

    div = det(dx, dy)
    if div == 0:
        return 0, 0

    d = (det(a, b), det(c, d))
    x = det(d, dx) / div
    y = det(d, dy) / div
    return x, y


# puts any lidar index to correct range
def getNormilizedLidarIndex(i):
    i = int(i)
    while i >= 300:
        i -= 300
    while i < 0:
        i += 300
    return i


# puts any angle to correct range (radians)
def getNormilizedAngle(a):
    while a >= np.pi:
        a -= np.pi * 2
    while a < -np.pi:
        a += np.pi * 2
    return a


# changes type of a vector to int
# useful in drawing
def getIntVector(a):
    return int(a[0]), int(a[1])
