import cv2
import glob
import math
import pickle

import apriltag
import solution.apriltag1
import matplotlib
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
from numpy import sqrt

import rclpy
from std_msgs.msg import *

detector = apriltag.Detector()

rclpy.init()
node = rclpy.create_node('apriltag')

pub_rot = node.create_publisher(Float32MultiArray, '/qr/xyz', 10)
pub_id = node.create_publisher(Int64, '/qr/id', 10)
pub_center = node.create_publisher(Float32MultiArray, '/qr/center', 10)
pub_dist = node.create_publisher(Float32, '/qr/dist', 10)


def filterCalman(smcam):
    Q = 0.8
    R = 0.2
    T = 5.0  # начальные дисперсии шумов(выбраны произвольно) и нулевые значения  переменных.
    P = Q * R / (Q + R)  # первая оценка дисперсий шумов.
    x1 = 1 - math.exp(-1 / T) + smcam * math.exp(-1 / T)  # модельная функция для x.
    ravn1 = np.random.uniform(0, 2 * sqrt(Q))  # равномерное распределение #для шума с дисперсией Q.
    ravn2 = np.random.uniform(0, 2 * sqrt(R))  # равномерное распределение #для шума с дисперсией R.
    P = P - (P ** 2) / (P + Q + R)  # переход в новое состояние для x.
    smcam = (P * x1 + smcam * R) / (P + R)  # новое состояние x.
    return smcam


def sender(rot, id, center, dist):
    msg = Float32MultiArray(data=rot)
    pub_rot.publish(msg)

    msg = Int64(data=id)
    pub_id.publish(msg)

    msg = Float32MultiArray(data=center)
    pub_center.publish(msg)

    msg = Float32(data=dist)
    pub_dist.publish(msg)


def rot_params_rv(rvecs):
    from math import pi, atan2, asin
    R, jac = cv2.Rodrigues(rvecs)

    roll = 180 * atan2(-R[2][1], R[2][2]) / pi
    pitch = 180 * asin(R[2][0]) / pi
    yaw = 180 * atan2(-R[1][0], R[0][0]) / pi
    rot_params = [roll, pitch, yaw]
    return rot_params


matplotlib.use("TkAgg")
cell_size = 0.23
objp = np.zeros((9 * 6, 3), np.float32)
objp[:, :2] = np.mgrid[0:9, 0:6].T.reshape(-1, 2) * cell_size

# Arrays to store object points and image points from all the images
objpoints = []  # 3D points in real world space
imgpoints = []  # 2D points in image plane

images = glob.glob("/workspace/src/solution/solution/bb/*.jpg")

# Step through the list and search for grid corners
for image in images:
    img = mpimg.imread(image)
    gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

    # Find the grid corners
    ret, corners = cv2.findChessboardCorners(gray_img, (9, 6), None)

    # If found, append object points and image points to the respective arrays
    if ret:
        objpoints.append(objp)
        imgpoints.append(corners)

cv2.destroyAllWindows()

img = mpimg.imread("/workspace/src/solution/solution/bb/1_bb.jpg")
img_size = (img.shape[1], img.shape[0])

ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, img_size, None, None)

'''
# Print the camera calibration result
print("Distortion Coefficients:")
print("dist = \n" + str(dist))
print("")

print("Camera Intrinsic Parameters:")
print("mtx = \n" + str(mtx))
print("")

print("Camera Extrinsic Parameters:")
print("rvecs = \n" + str(rvecs))
print("")
print("tvecs = \n" + str(tvecs))
'''

# Save the camera calibration result for later use
camera_pickle = {"mtx": mtx, "dist": dist, "rvecs": rvecs, "tvecs": tvecs}
pickle.dump(camera_pickle, open("Camera Parameters.p", "wb"))

img = mpimg.imread("bb/1_bb.jpg")  # Read in a test calibration image
undistorted_image = cv2.undistort(img, mtx, dist, None, mtx)  # Generate the undistorted image

f, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 5))
f.tight_layout()
# ax1.imshow(img)
ax1.set_title("Original Image", fontsize=20)
# ax2.imshow(undistorted_image)
ax2.set_title("Undistorted Image", fontsize=20)
plt.subplots_adjust(left=0.0, right=1.0, top=0.9, bottom=0.0)
tag_size = 0.159

cap = cv2.VideoCapture(0)
cap.set(3, 1280)
cap.set(4, 720)
ob_pt1 = [-tag_size / 2, -tag_size / 2, 0.0]
ob_pt2 = [tag_size / 2, -tag_size / 2, 0.0]
ob_pt3 = [tag_size / 2, tag_size / 2, 0.0]
ob_pt4 = [-tag_size / 2, tag_size / 2, 0.0]
ob_pts = ob_pt1 + ob_pt2 + ob_pt3 + ob_pt4
object_pts = np.array(ob_pts).reshape(4, 3)

opoints = np.array([
    -1, -1, 0,
    1, -1, 0,
    1, 1, 0,
    -1, 1, 0,
    -1, -1, -2 * 1,
    1, -1, -2 * 1,
    1, 1, -2 * 1,
    -1, 1, -2 * 1,
]).reshape(-1, 1, 3) * 0.5 * tag_size

while True:
    _, frame = cap.read()

    if cv2.waitKey(1) & 0xFF == ord("q"):  # press Q to exit:
        break

    h, w = frame.shape[:2]
    frame_d = undistorted_image = cv2.undistort(frame, mtx, dist, None, mtx)  # Generate the undistorted image
    newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w, h), 1, (w, h))
    mapx, mapy = cv2.initUndistortRectifyMap(mtx, dist, None, newcameramtx, (w, h), 5)
    dst = cv2.remap(frame, mapx, mapy, cv2.INTER_LINEAR)
    x, y, w, h = roi
    dst = dst[y:y + h, x:x + w]
    result, overlay, cor, = apriltag1.detect_tags(dst,
                                                  detector,
                                                  camera_params=(
                                                      newcameramtx[0][0], newcameramtx[1][1], newcameramtx[0][2],
                                                      newcameramtx[1][2]),
                                                  tag_size=0.159,
                                                  vizualization=1,
                                                  verbose=0,
                                                  annotation=False
                                                  )
    if result:
        qr_center = [int(result[0][6][0]), int(result[0][6][1])]

        rect_side = math.sqrt(
            (result[0][7][0][0] - result[0][7][1][0]) ** 2 + (result[0][7][0][1] - result[0][7][1][1]) ** 2)
        D2 = result[0][7].reshape(4, 2)

        good, prvecs, ptvecs = cv2.solvePnP(object_pts, D2, mtx, dist, flags=cv2.SOLVEPNP_ITERATIVE)

        xyz = rot_params_rv(prvecs)

        distance_sm = filterCalman(abs(math.cos(math.radians(xyz[0]))) * (1 / (rect_side * 0.068) * 100))

        if xyz and result[0][1] and qr_center and distance_sm:
            sender(xyz, result[0][1], qr_center, distance_sm)
    else:
        sender([], 0, [], 0.0)

    # cv2.imshow("ss", overlay)
