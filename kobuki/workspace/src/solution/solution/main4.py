#!/usr/bin/python3
print("-> main.py")

import os.path

import cv2 as cv
import rclpy
import time

import solution.globals as g

from solution.lidar import *
from solution.map import *
from solution.math import *
from solution.modeset import *
from solution.move import *
from solution.overlay import *
from solution.qrcodes import *
from solution.walls import *
from solution.bfs_algorithm import *
from solution.trajectory_planning import TrajectoryPlanner

from cv_bridge import CvBridge
from nav_msgs.msg import Odometry
from sensor_msgs.msg import Image, LaserScan
from std_msgs.msg import Float32MultiArray, Int64, Float32


# ros callbacks
def callbackCameraColor(data):
    g.imgColRos = data


def callbackLidar(data):
    g.lidar = data


def callbackOdometry(data):
    g.odom = data

    position = g.odom.pose.pose.position
    g.pos = (position.y, position.x)

    p = mapTranslateFrom(g.pos_offset)
    g.pos = (g.pos[0] + p[0], g.pos[1] + p[1])

    orientation = g.odom.pose.pose.orientation
    x, y, z = getAngleFromQuaternion(orientation.w, orientation.x, orientation.y, orientation.z)

    g.rot = z + g.ang_offset


def callback_xyz(data):
    g.qr_rot = getAngleFromQuaternion(data[0], data[1], data[2], data[3])[1]


def callback_id(data):
    g.qr_id = data


def callback_center(data):
    g.qr_center = data


def callback_dist(data):
    g.qr_dist = data


def main():
    global my_space, my_walls

    rclpy.init()
    g.node = rclpy.create_node("solution")

    subCameraColor = g.node.create_subscription(Image, "/tb4/camera/color/image_raw", callbackCameraColor,
                                                rclpy.qos.qos_profile_sensor_data)
    subLidar = g.node.create_subscription(LaserScan, "/tb4/scan", callbackLidar, rclpy.qos.qos_profile_sensor_data)
    subOdom = g.node.create_subscription(Odometry, "/tb4/odom", callbackOdometry, rclpy.qos.qos_profile_sensor_data)

    sub_xyz = g.node.create_subscription(Float32MultiArray, "/tb4/qr/xyz",
                                         callback_xyz,
                                         rclpy.qos.qos_profile_sensor_data)
    sub_qr_id = g.node.create_subscription(Int64, "/tb4/qr/id",
                                           callback_id,
                                           rclpy.qos.qos_profile_sensor_data)
    sub_qr_center = g.node.create_subscription(Float32MultiArray, "/tb4/qr/center",
                                               callback_center,
                                               rclpy.qos.qos_profile_sensor_data)
    sub_qr_dist = g.node.create_subscription(Float32, "/tb4/qr/dist",
                                             callback_dist,
                                             rclpy.qos.qos_profile_sensor_data)


    moveInit()

    bridge = CvBridge()

    overlayInit()

    print("Examining map")
    g.planner = TrajectoryPlanner("/workspace/src/solution/solution/maps")
    g.planner.examine_map(mapGetMatrix())
    print("Examining map END")

    try:
        while rclpy.ok():
            rclpy.spin_once(g.node)

            # skip processing with no data
            if g.imgColRos is None:
                print("Skip (imgColRos is None)")
                time.sleep(0.2)
                continue

            if g.lidar is None:
                print("Skip (lidar is None)")
                time.sleep(0.2)
                continue

            if g.odom is None:
                print("Skip (odom is None)")
                time.sleep(0.2)
                continue

            modesetController()

            lidar = lidarRotate(g.lidar, (-g.rot))

            overlayReset()
            overlayDrawLidar(lidar, mapTranslateTo(g.pos))

            if g.path is not None:
                print(g.path)
                for pt in g.path:
                    cv.circle(overlayGetMatrix(), pt, 3, (0, 255, 128), -1)

            # overlayDisplay()

            # draw my lidar
            # my_space, my_walls, fine_map = draw_lidar(my_space, my_walls, lidar, mapTranslateTo(g.pos))

            # imgColCv = bridge.imgmsg_to_cv2(g.imgColRos, "bgr8")
            # cv.imshow("Color", imgColCv)

            # cv.imshow("my MAP", my_space)
            # cv.imshow("my WALLS", my_walls)
            # cv.imshow("i am FINE", fine_map)
            # cv.pollKey()

            modesetDump()
            moveDump()
            g.dump()

    except KeyboardInterrupt:
        print("\nInterrupted")
        pass

    finally:
        qrcodesDump()
        move(0.0)
        rotate(0.0)
        moveExecute()

        if os.path.exists(maps_folder):
            cv.imwrite(maps_folder + "space.png", my_space)
            cv.imwrite(maps_folder + "walls.png", my_walls)
        else:
            print(f"No save folder: {maps_folder}")


if __name__ == "__main__":
    main()
