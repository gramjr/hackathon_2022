#!/usr/bin/python3
print("-> bfs_algorithm.py")

from abc import ABC, abstractmethod

class Node(ABC):
    """
      This class used to represent a Node in the graph
      It's important to implement this interface in order to make the class BFS more general
      and to use it for various problems
      ...


      Methods
      -------
      __eq__(self, other)
          Determines if two nodes are equal or not

      is_the_solution(self)
          Determines if the current node is the solution of the problem

      def is_the_solution(self)
          Extends the current node according to the rules of the problem

      __str__(self)
          Prints the node data
    """

    @abstractmethod
    def __eq__(self, other):
        pass

    @abstractmethod
    def is_the_solution(self, state):
        pass

    @abstractmethod
    def extend_node(self):
        pass

    @abstractmethod
    def __str__(self):
        pass


class BFS:
    """
      This class used to represent the  Breadth First Search algorithm (BFS)

      ...

      Attributes
      ----------
      start_state : Node
          represent the initial state of the problem
      final_state : Node
          represent the final state (target) of the problem
      frontier : List
          represents the stack and is initialized with the start node
      checked_nodes : List
          represents the list of nodes that have been visited throughout the algorithm execution
      number_of_steps : Integer
          Keep track of the algorithm's number of steps
      path : List
          represents the steps from the initial state to the final state

      Methods
      -------
      insert_to_frontier(self, node)
          Insert a new node to the frontier. In this algorithm the frontier is a queue, so each new element is inserted to end of the data structure

      remove_from_frontier(self)
          Remove the first element from the frontier, following the FIFO technic. The removed node is added to the checked_node list

      remove_from_frontier(self)
          check if the frontier is empty

      search(self)
          Implements the core of algorithm. This method searches, in the search space of the problem, a solution
      """

    def __init__(self, start, final):
        self.start_state = start
        self.final_state = final
        self.frontier = [self.start_state]
        self.checked_nodes = []
        self.number_of_steps = 0
        self.path = []

    def insert_to_frontier(self, node):
        """
          Insert a node at the end of the frontier

          Parameters
          ----------
          node : Node
              The node of the problem that will be added to the frontier
        """
        self.frontier.append(node)

    def remove_from_frontier(self):
        """
          Remove a node from the beginning of the frontier
          Then add the removed node to the checked_nodes list

          Returns
          -------
          Node
            the first node of the frontier
        """
        first_node = self.frontier.pop(0)
        self.checked_nodes.append(first_node)
        return first_node

    def frontier_is_empty(self):
        """
          Check if the frontier id empty, so no solution found

          Returns
          -------
          Boolean
            True if the frontier is empty
            False if the frontier is not empty
        """
        if len(self.frontier) == 0:
            return True
        return False

    def search(self):
        """
          Is the main algorithm. Search for a solution in the solution space of the problem
          Stops if the frontier is empty, so no solution found or if find a solution.
        """
        while True:

            self.number_of_steps += 1

            # print(f"Step: {self.number_of_steps}, Frontier Size: {len(self.frontier)} ")
            if self.frontier_is_empty():
                # print(f"No Solution Found after {self.number_of_steps} steps!!!")
                return None

            selected_node = self.remove_from_frontier()

            # check if the selected_node is the solution
            if selected_node.is_the_solution(self.final_state):
                # print(f"Solution Found in {self.number_of_steps} steps")
                return str(selected_node)

            # extend the node
            new_nodes = selected_node.extend_node()

            # add the extended nodes in the frontier
            if len(new_nodes) > 0:
                for new_node in new_nodes:
                    if new_node not in self.frontier and new_node not in self.checked_nodes:
                        self.insert_to_frontier(new_node)


class MazeNode(Node):
    """
      This class used to represent the node of a maze
      ...
      Attributes
      ----------
      graph : Dictionary
          represent the graph
      value : String
          represents the id of the vertex
      parent : MazeNode
          represents the parent of the current node

      Methods
      -------
      __eq__(self, other)
          Determines if the current node is the same with the other
      is_the_solution(self, final_state)
          Checks if the current node is the solution
      extend_node(self)
          Extends the current node, creating a new instance of MazeNode for each edge starts from current node
      _find_path(self)
          Find the path (all verticies and edges from the intitial state to the final state)
      __str__(self)
          Returns the solution of the maze, the whole path vertex by vertex in order to be printed properly.
    """

    def __init__(self, graph, value):
        self.graph = graph
        self.value = value
        self.parent = None

    def __eq__(self, other):
        """
          Check if the current node is equal with the other node.
          Parameters
          ----------
          Other : MazeNode
              The other vertex of the graph
          Returns
          -------
          Boolean
            True: if both verticies are the same
            False: If verticies are different
        """
        if isinstance(other, MazeNode):
            return self.value == other.value
        return self.value == other

    def is_the_solution(self, final_state):
        """
          Checks if the current node is the solution
          Parameters
          ----------
          final_state : MazeNode
              The target vertex (final state) of the graph
          Returns
          -------
          Boolean
            True: if both verticies are the same, so solution has been found
            False: If verticies are different, so solution has not been found
        """
        return self.value == final_state

    def extend_node(self):
        """
          Extends the current node, creating a new instance of MazeNode for each edge starts from the current node
          Returns
          -------
          List
            List with all valid new nodes
        """
        children = [MazeNode(self.graph, child) for child in self.graph[self.value]]
        for child in children:
            child.parent = self
        return children

    def _find_path(self):
        """
          Find the path, all verticies and edges from the intitial state to the final state
          Returns
          -------
          List
            List with all nodes fron start to end in a row
        """
        path = []
        current_node = self
        while current_node.parent is not None:
            path.insert(0, current_node.value)
            current_node = current_node.parent
        path.insert(0, current_node.value)
        return path

    def __str__(self):
        """
          Returns the solution of the maze, the whole path vertex by vertex as well as the path lenght, in order to be printed properly.
          Returns
          -------
          str
            the solution of the problem
        """
        return str(self._find_path())
