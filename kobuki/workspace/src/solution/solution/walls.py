#!/usr/bin/python3
print("-> walls.py")

import numpy as np
import cv2 as cv

import solution.globals as g
from solution.math import *
from solution.map import *

wallsDetector = cv.createLineSegmentDetector(cv.LSD_REFINE_ADV, 0.9, 1.04, 2.0, 22.1, 0, 0.4)


# draws all lidar non-infinity values as small circles on separate matrix
def wallsDraw(lidar, matrix=np.zeros((mapSize, mapSize), dtype=np.uint8), color=255):
    for i in range(0, 300):
        val0 = lidar.ranges[i]
        val1 = lidar.ranges[i - 1]
        if np.isinf(val0) or np.isinf(val1):
            continue

        cv.rectangle(matrix, getIntVector(getLidarPointTrace(i, val0 * 10)),
                     getIntVector(getLidarPointTrace(getNormilizedLidarIndex(i - 1), val1 * 10)), color, 3)
    # cv.imshow("Walls", matrix)
    return matrix


# recognizes all line segments
def wallsRecognize(matrix):
    walls = []

    '''
    lines, width, prec, nfa = wallsDetector.detect(matrix)

    if lines is not None:
        matagain = np.empty_like(matrix)
        matagain.fill(255)

        for i in lines:
            for j in i:
                cv.line(matagain, (int(j[0]), int(j[1])), (int(j[2]), int(j[3])), 0)

        cv.imshow("d", matagain)

        lines = cv.HoughLinesP(matrix, 1.0, np.pi / 180, 50, 2, 60, 10)
        if lines is not None:
            for i in lines:
                l = i[0]
                walls.append(((l[0], l[1]), (l[2], l[3])))

    return walls
    '''

    # lines = cv.HoughLinesP(matrix, 1.0, np.pi / 180, 50, 2, 60, 10)

    # if lines is not None:
    # for i in range(0, len(lines)):
    # l = lines[i][0]
    # walls.append(((l[0], l[1]), (l[2], l[3])))

    # return walls

    lines, width, prec, nfa = wallsDetector.detect(matrix)

    if lines is not None:
        # det.drawSegments(mat, lines)
        for i in lines:
            for j in i:
                walls.append(((int(j[0]), int(j[1])), (int(j[2]), int(j[3]))))

    '''
    print("[WALLS RAW]")
    #for i in range(len(walls)):
    #     print(walls[i])
    print(walls)
    print("rrrrr")
    errorpixel=20
    for i in range(len(walls)):
        for j in range(len(walls)):
            if(i!=j and
               ( (abs(walls[i][0][0]-walls[j][0][0])<errorpixel and
                abs(walls[i][0][1]-walls[j][0][1])<errorpixel and
                abs(walls[i][1][0]-walls[j][1][0])<errorpixel and
                abs(walls[i][1][1]-walls[j][1][1])<errorpixel ) or
                (abs(walls[i][0][0]-walls[j][1][0])<errorpixel and
                abs(walls[i][0][1]-walls[j][1][1])<errorpixel and
                abs(walls[i][1][0]-walls[j][0][0])<errorpixel and
                abs(walls[i][1][1]-walls[j][0][1])<errorpixel )

                #ВСЕ ВОМОЖНЫЕ ПОЗИЦИИ ВЕКТОРОВ, УДАЛЕНИЕ КОТОРЫЕ НЕ ПОДХОДЯТ ПОД УГОЛ, УВЕЛИЧИТЬ КАРТУ, СОЗДАТЬ ЛИСТ С НОРМАЛЬНЫМИСТЕНКАМИ И ЕСЛИ ТАМ УЖЕ ЕСТЬ ЭТА, ТО ЕЕ НЕ РИСОВАТЬ
                )):
                print(walls[i])
    '''

    # print("[WALLS RAW]")
    # print(walls)
    return walls


# check for near lines and filter only one
def wallsFilter(walls):
    out = []

    for i in walls:
        dmax = 5
        dx = abs(i[0][0] - i[1][0])
        dy = abs(i[0][1] - i[1][1])

        if dx < dmax or dy < dmax:
            out.append(i)

    '''
    maxError = 30
    out0 = []
    outc = []
    for c, i in enumerate(out):
        for d, j in enumerate(out):

            #ВСЕ ВОМОЖНЫЕ ПОЗИЦИИ ВЕКТОРОВ, УДАЛЕНИЕ КОТОРЫЕ НЕ ПОДХОДЯТ ПОД УГОЛ, УВЕЛИЧИТЬ КАРТУ, СОЗДАТЬ ЛИСТ С НОРМАЛЬНЫМИ СТЕНКАМИ И ЕСЛИ ТАМ УЖЕ ЕСТЬ ЭТА, ТО ЕЕ НЕ РИСОВАТЬ
            if(i != j and (c not in outc) and (d not in outc) and
                ((abs(i[0][0] - j[0][0]) < maxError and
                  abs(i[0][1] - j[0][1]) < maxError and
                  abs(i[1][0] - j[1][0]) < maxError and
                  abs(i[1][1] - j[1][1]) < maxError)
                  or
                 (abs(i[0][0] - j[1][0]) < maxError and
                  abs(i[0][1] - j[0][1]) < maxError and
                  abs(i[1][0] - j[0][0]) < maxError and
                  abs(i[1][1] - j[1][1]) < maxError))):
                out0.append(i)
                outc.append(c)
                outc.append(d)

            if(i != j and (c not in outc) and (d not in outc) and
                ((getDistance2P(i[0], j[0]) < maxError and
                  getDistance2P(i[0], j[1]) < maxError and
                  getDistance2P(i[1], j[0]) < maxError and
                  getDistance2P(i[1], j[1]) < maxError)
                  or
                 (getDistance2P(i[0], j[1]) < maxError and
                  getDistance2P(i[0], j[0]) < maxError and
                  getDistance2P(i[1], j[0]) < maxError and
                  getDistance2P(i[1], j[1]) < maxError))):
                out0.append(i)
                outc.append(c)
                outc.append(d)

    for i in walls:
        exists = False

        for c, j in enumerate(out):
            if i == j:
                exists = True
                break

            d0 = getDistance2P(i[0], j[0])
            d1 = getDistance2P(i[1], j[1])

            #if line point swapped
            d2 = getDistance2P(i[0], j[1])
            d3 = getDistance2P(i[1], j[0])

            #if distances between lines point less than 10
            #than it's the same line and it's already exists in output array
            dMax = 5
            if ((d0 < dMax) and (d1 < dMax)) or ((d2 < dMax) and (d3 < dMax)):
                exists = True
                out[c] = (
                    (int((i[0][0] + j[0][0]) / 2), int((i[0][1] + j[0][1]) / 2)),
                    (int((i[1][0] + j[1][0]) / 2), int((i[1][1] + j[1][1]) / 2))
                )
                break

        #if such line already exist in out array do nothing
        if not exists:
            out.append(i)
    '''
    # print("[WALLS FLT]")
    # print(out)
    return out


def wallsReflect(walls, color, size):
    matrix = np.zeros(size, dtype=np.uint8)
    for i in walls:
        cv.line(matrix, i[0], i[1], color)

    return matrix


def wallsCheckIntersects(walls, a, b):
    res = False
    for i in walls_:
        args = (a, b, (i[0], i[1]), (i[2], i[3]))
        if getTwoLineSegmentsIntersection(*args):
            res = True
            vec = getTwoLineIntersectionPoint(*args)
            print(vec)
            cv.circle(g.overlay, (int(vec[0]), int(vec[1])), 3, (0, 0, 0), -1)

        color = (0, 255, 0)
        if res:
            color = (0, 0, 255)

    cv.line(g.overlay, (int(x), int(y)), (mousePos_[0], mousePos_[1]), color)
    cv.circle(g.overlay, mousePos_, 3, (0, 0, 0), -1)
