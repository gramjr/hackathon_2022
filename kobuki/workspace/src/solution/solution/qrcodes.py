#!/usr/bin/python3
print("-> qrcodes.py")

from pyzbar.pyzbar import decode

qrSet = set()


def qrcodesUpdate(image):
    global qrSet

    qrs = decode(imgColCv_)

    print("qrs\t:")
    for i in qrs:
        qrStr = i.data.decode("UTF-8")

        print(qrStr)
        if qrStr not in qrSet:
            qrSet.add(qrStr)


def qrcodesDump():
    print("\n[QRCODES]")
    if len(qrSet) == 0:
        print("<Empty>")
    else:
        for j in sorted(qrSet):
            print("    -", j)
    print("\n")
