#!/usr/bin/python3
print("-> overlay.py")

import cv2 as cv

import solution.globals as g
from solution.map import *
from solution.math import *

overlayDisplayName = "Overlay"
overlayMatrix = np.empty((mapSize, mapSize, 3), dtype=np.uint8)


def overlayCallbackMouse(event, x, y, flags, param):
    if event == cv.EVENT_MBUTTONDOWN:
        if g.click_num == 0:
            g.pos_offset = (int(x), int(y))
            g.click_num += 1
        elif g.click_num == 1:
            g.ang_offset = getNormilizedAngle(getAngle2P(g.pos_offset, (int(x), int(y))) + np.pi)
            g.click_num = 0

    if event == cv.EVENT_LBUTTONDOWN:
        g.mousePos = (int(x), int(y))
        g.path = list(g.planner.path(mapTranslateTo(g.pos), g.mousePos))


def overlayInit():
    cv.namedWindow(overlayDisplayName)
    cv.setMouseCallback(overlayDisplayName, overlayCallbackMouse)


def overlayGetMatrix():
    return overlayMatrix


def overlayReset():
    global overlayMatrix

    # draw cool lines on overlay
    sx, sy = mapSize, mapSize
    cx, cy = sx / 2, sy / 2

    overlayMatrix.fill(255)
    overlayMatrix = cv.cvtColor(mapGetMatrix(), cv.COLOR_GRAY2BGR)

    cv.line(overlayMatrix, (int(0), int(cy)), (int(sx), int(cy)), (0, 0, 0), 1)
    cv.line(overlayMatrix, (int(cx), int(0)), (int(cx), int(sy)), (0, 0, 0), 1)


def overlayDrawLidar(lidar, where):
    for c, i in enumerate(lidar.ranges):
        if np.isinf(i):
            i = 10.0

        x = where[0] + np.sin(c * lidar.angle_increment) * i * 10
        y = where[1] + np.cos(c * lidar.angle_increment) * i * 10

        cv.line(overlayMatrix, getIntVector(where), (int(x), int(y)), (255, 0, 0))


def draw_lidar(map_, walls, lidar, where):
    for c, i in enumerate(lidar.ranges):
        if not np.isinf(i):
            x = where[0] + np.sin(c * lidar.angle_increment) * i * 10
            y = where[1] + np.cos(c * lidar.angle_increment) * i * 10

            cv.circle(walls, (int(x), int(y)), 2, (0, 0, 0))

            cv.line(map_, getIntVector(where), (int(x), int(y)), (255, 255, 255))

    _, map_ = cv.threshold(map_, 0, 255, cv.THRESH_BINARY)
    _, walls = cv.threshold(walls, 0, 255, cv.THRESH_BINARY)

    fine_map = cv.bitwise_and(walls, map_)

    # map_ = cv.morphologyEx(map_, cv.MORPH_OPEN, kernel)
    # walls = cv.morphologyEx(walls, cv.MORPH_OPEN, kernel)

    return map_, walls, fine_map


def lidarDisplay(lidar):
    mat = np.zeros((500, 500), dtype=np.uint8)

    cy, cx = mat.shape
    cx /= 2
    cy /= 2

    for c, i in enumerate(lidar.ranges):
        if np.isinf(i):
            i = 10.0

        x = cx - np.sin(lidar.angle_min + c * lidar.angle_increment) * i * 25
        y = cy - np.cos(lidar.angle_min + c * lidar.angle_increment) * i * 25

        cv.line(mat, (int(cx), int(cy)), (int(x), int(y)), 255)
    cv.imshow("Lidar", mat)


def overlayDrawMachine(pos, rot):
    pass


def overlayDrawWalls(walls, color=(0, 0, 255)):
    for i in walls:
        cv.rectangle(overlayMatrix, i[0], i[1], color, 3)


def overlayDisplay():
    cv.circle(overlayMatrix, getIntVector(mapTranslateTo(g.pos)), 3, (0, 255, 0), -1)
    cv.circle(overlayMatrix, g.mousePos, 3, (255, 0, 255), -1)
    # cv.imshow(overlayDisplayName, overlayMatrix)
