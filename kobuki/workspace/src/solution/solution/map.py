#!/usr/bin/python3
print("-> map.py")

import os
import numpy as np
import cv2 as cv

maps_folder = "/workspace/src/solution/solution/maps/"
my_space_name = "space.png"
my_walls_name = "walls.png"
my_map_name = "map1.png"

mapSize = 600
mapMatrix = None

if os.path.exists(maps_folder + my_space_name) and False:
    my_space = cv.imread(maps_folder + my_space_name)
else:
    my_space = np.zeros((mapSize, mapSize))

if os.path.exists(maps_folder + my_walls_name) and False:
    my_walls = cv.imread(maps_folder + my_walls_name)
else:
    my_walls = np.zeros((mapSize, mapSize))

if os.path.exists(maps_folder + my_map_name):
    mapMatrix = cv.imread(maps_folder + my_map_name)
    mapMatrix = cv.cvtColor(mapMatrix, cv.COLOR_BGR2GRAY)
else:
    raise Exception(f"No {my_map_name} in {maps_folder}")


# map function from arduino
def __map(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min


# translates real coords to map ones
# takes vector
# returns vector
def mapTranslateTo(a):
    return __map(a[0], 6.5, -6.5, 0, mapSize), __map(a[1], 6.5, -6.5, 0, mapSize)


# translates map coords to real ones
# takes vector
# returns vector
def mapTranslateFrom(a):
    return __map(a[0], 0, mapSize, 6.5, -6.5), __map(a[1], 0, mapSize, 6.5, -6.5)


# reads from a map
# takes vector
# returns data
def mapTake(a):
    return mapMatrix[a[1], a[0]]


# writes to a map
# takes vector, data
# returns nothing
def mapWrite(a, data):
    mapMatrix[a[1], a[0]] = data


def mapGetMatrix():
    return mapMatrix


def mapSetMatrix(matrix):
    global mapMatrix
    mapMatrix = matrix


# displays map on separate window
# takes nothing
# returns nothing
def mapDisplay():
    pass
    # cv.imshow("Map", mapMatrix)
