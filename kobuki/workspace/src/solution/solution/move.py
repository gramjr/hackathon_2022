#!/usr/bin/python3

import solution.globals as g

from solution.math import *
from geometry_msgs.msg import Twist

print("-> move.py")

twist = Twist()
twist.linear.x = 0.0
twist.linear.y = 0.0
twist.linear.z = 0.0
twist.angular.x = 0.0
twist.angular.y = 0.0
twist.angular.z = 0.0
twistPub = None

__spd = 0.0


def moveInit():
    global twistPub
    twistPub = g.node.create_publisher(Twist, "/tb4/commands/velocity", 4)


def rotate(s):
    global twistPub
    twist.angular.z = s


def rotateTo(a):
    a = getNormilizedAngle(a + np.pi)

    e = a - g.rot

    if e > np.pi:
        e -= np.pi * 2
    if e < -np.pi:
        e += np.pi * 2

    print(e)
    print(a)
    print(g.rot)

    rotate(min(1.5, max(e * 1.5, -1.5)))
    return abs(e)


def move(s):
    global twistPub
    twist.linear.x = s


def spdC(t):
    global __spd
    __spd += (t - __spd) * 0.015
    return __spd


def moveTo(a):
    global __spd

    b = mapTranslateTo(g.pos)
    d = getDistance2P(a, b)
    if rotateTo(getAngle2P(b, a)) < 0.1 and d > 5:
        move(spdC(0.3))
    else:
        move(spdC(0.0))

    if d < 2:
        __spd = 0.0
    else:
        __spd = max(0.055, __spd)
    move(__spd)

    if __spd > 0.05:
        d += 2

    print(d)
    return d


def moveExecute():
    twistPub.publish(twist)


def moveDump():
    print("[MOVE]")
    print("linear\t:", twist.linear.x)
    print("angular\t:", twist.angular.z)
