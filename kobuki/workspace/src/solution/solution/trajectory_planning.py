#!/usr/bin/python3
print("-> trajectory_planning.py")

import cv2
import numpy as np
import os
import pickle

from time import time
from itertools import combinations
from collections import deque

from solution.bfs_algorithm import BFS, MazeNode

robot = (300, 300)
new_robot = (50, 50)

robot_diameter = 10


def draw_H_map():
    array = np.zeros((1000, 1000, 1), dtype = np.uint8)

    poly = np.array([(100, 100), (100, 500), (300, 500), (300, 600), (100, 600),
                     (100, 900), (900, 900), (900, 600), (500, 600), (500, 500),
                     (500, 500), (900, 500), (900, 100)], dtype = np.int32)
    cv2.fillPoly(array, [poly], 255)

    return array


class TrajectoryPlanner:
    def __init__(self, save_folder):
        self.smart_map = None
        self.rooms = None
        self.rooms_centers = None
        self.transitions = None

        self.low_threshold = 50
        self.high_threshold = 150

        self.rho = 1               # distance resolution in pixels of the Hough grid
        self.theta = np.pi / 180   # angular resolution in radians of the Hough grid
        self.threshold = 15        # minimum number of votes (intersections in Hough grid cell)
        self.min_line_length = 10  # minimum number of pixels making up a line
        self.max_line_gap = 50     # maximum gap in pixels between connectable line segments

        self.wall_width = 3
        self.transition_error = 0

        self.graph = dict()

        self.save_folder = save_folder
        self.save_file_name = "/plan.pkl"

        if os.path.exists(self.save_folder + self.save_file_name):
            self.load()
            print("MAP DATA LOADED")

    def examine_map(self, bw_map):
        if os.path.exists(self.save_folder + self.save_file_name):
            return

        edges = cv2.Canny(bw_map, self.low_threshold, self.high_threshold)
        smart_map = np.copy(bw_map)  # creating a copy, so we won't change bw_map

        # run Hough on edge detected image
        lines = cv2.HoughLinesP(edges,
                                self.rho,
                                self.theta,
                                self.threshold,
                                np.array([]),
                                self.min_line_length,
                                self.max_line_gap)

        # draw detected line black
        if lines is None:
            return None

        for line in lines:
            for x1, y1, x2, y2 in line:
                length_x = abs(x1 - x2)
                length_y = abs(y1 - y2)
                length = np.hypot(length_x, length_y)
                angle = np.arctan(length_x / length_y)

                cv2.line(smart_map,
                         (x1 - int(length * np.sin(angle) * 100), y1 - int(length * np.cos(angle)) * 100),
                         (x2 + int(length * np.sin(angle) * 100), y2 + int(length * np.cos(angle)) * 100),
                         0, self.wall_width)

        rooms = []  # centers, widths and heights
        rooms_centers = []  # centers, widths and heights

        # find centers of "rooms"
        contours, _ = cv2.findContours(smart_map, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[-2:]
        for cnt in contours:
            x, y, w, h = cv2.boundingRect(cnt)
            c = x + w // 2, y + h // 2
            rooms.append((c, (w, h)))
            rooms_centers.append(c)
            self.graph[(x + w // 2, y + h // 2)] = set()
            # cv2.circle(smart_map, (x + w // 2, y + h // 2), 10, 100)

        # find rooms transitions
        transitions = []
        smart_copy = np.copy(smart_map)
        for room1, room2 in combinations(rooms, 2):
            if room1 != room2:
                c1, c2 = room1[0], room2[0]
                x1, y1 = c1
                x2, y2 = c2
                w1, w2 = room1[1][0] // 2, room2[1][0] // 2
                h1, h2 = room1[1][1] // 2, room2[1][1] // 2

                # defining which room is shorter
                if w1 < w2:
                    center1 = c1
                    center2 = c2
                else:
                    center1 = c2
                    center2 = c1

                new_y1 = center1[1]
                new_y2 = center2[1]

                l1, r1 = x1 - w1, x1 + w1
                l2, r2 = x2 - w2, x2 + w2

                length_y = abs(new_y1 - new_y2)

                false_transitions = []

                # checking vertical lines
                for new_x in range(max(l1, l2), min(r1, r2)):
                    bw_line = bw_map[new_y1: new_y2, new_x: new_x + 1]
                    arrays = np.where(bw_line == 255)

                    # if entire line is white
                    if arrays[0].size == length_y:
                        smart_line = smart_copy[new_y1: new_y2, new_x: new_x + 1]
                        diff = cv2.bitwise_xor(bw_line, smart_line)

                        avg_y = np.average(np.where(diff == 255)[0])

                        # if difference exists
                        if not np.isnan(avg_y):
                            avg_y = int(avg_y)
                            cv2.line(smart_map, (new_x, new_y1), (new_x, new_y2), 255)
                            false_transitions.append((new_x, avg_y + new_y1))

                if false_transitions:
                    transition = tuple(map(int, np.average(false_transitions, axis=0)))
                    transitions.append(transition)
                    self.graph[room1[0]].add(transition)
                    self.graph[room2[0]].add(transition)
                    if transition not in self.graph:
                        self.graph[transition] = set()
                    self.graph[transition].add(room1[0])
                    self.graph[transition].add(room2[0])
                    continue

                false_transitions = []

                # defining which room is lower
                if h1 < h2:
                    center1 = c1
                    center2 = c2
                else:
                    center1 = c2
                    center2 = c1

                new_x1 = center1[0]
                new_x2 = center2[0]

                down1, up1 = y1 - h1, y1 + h1
                down2, up2 = y2 - h2, y2 + h2

                length_x = abs(new_x1 - new_x2)

                # checking horizontal lines
                for new_y in range(max(down1, down2), min(up1, up2)):
                    bw_line = bw_map[new_y: new_y + 1, new_x1: new_x2]
                    arrays = np.where(bw_line == 255)

                    # if entire line is white
                    if arrays[0].size == length_x:
                        smart_line = smart_copy[new_y: new_y + 1, new_x1: new_x2]
                        diff = cv2.bitwise_xor(bw_line, smart_line)

                        avg_x = np.average(np.where(diff == 255)[1])

                        if not np.isnan(avg_x):
                            avg_x = int(avg_x)
                            cv2.line(smart_map, (new_x1, new_y), (new_x2, new_y), 255)
                            false_transitions.append((avg_x + new_x1, new_y))

                if false_transitions:
                    transition = tuple(map(int, np.average(false_transitions, axis=0)))
                    transitions.append(transition)
                    self.graph[room1[0]].add(transition)
                    self.graph[room2[0]].add(transition)
                    if transition not in self.graph:
                        self.graph[transition] = set()
                    self.graph[transition].add(room1[0])
                    self.graph[transition].add(room2[0])

        self.rooms = rooms
        self.rooms_centers = np.asarray(rooms_centers)
        self.transitions = transitions
        self.smart_map = smart_map

        self.save()

        # for key, ls in self.graph.items():
        #     print(f"{key}: {ls}")

        # draw for debugging
        # for room in rooms:
        #    x, y = room[0]
        #    cv2.circle(smart_map, (x, y), 10, 100)

        # for pt in transitions:
        #    cv2.circle(smart_map, pt, 5, 100, -1)

        # cv2.imshow('sm', smart_map)

    # def _line(self):
    #     cv2.line(self.zeros, self.pt1, self.pt2, 1)
    #
    #     arrays = np.where(self.bw_map + self.zeros == 1)
    #
    #     if arrays[0].size > 0:
    #         top = arrays[1][0], arrays[0][0]
    #         down = arrays[1][-1], arrays[0][-1]
    #
    #         return top, down
    #
    #     else:
    #         return self.pt1, self.pt2
    #
    # def _avoid(self, inters):
    #     pt1, pt2 = inters[0], inters[1]
    #
    #     length_x = abs(pt1[0] - pt2[0])
    #     length_y = abs(pt1[1] - pt2[1])
    #
    #     # calculate new point for turned line on 90 degrees
    #     pt2 = pt2[0] + length_y * 1000, pt1[1] + length_x * 1000
    #
    #     return pt1, pt2

    def find_nearest_room(self, pt):
        diff = np.abs(self.rooms_centers - pt)

        x = deque()
        y = deque()
        for p in diff:
            x.append(p[0])
            y.append(p[1])

        idx = np.argmin(np.hypot(x, y))
        return self.rooms_centers[idx]

    def path(self, pt1, pt2):
        room1 = self.find_nearest_room(pt1)
        room2 = self.find_nearest_room(pt2)

        bfs = BFS(MazeNode(self.graph, tuple(room1)), tuple(room2))
        res = eval(bfs.search())

        return np.array([*res, pt2])

    def save(self):
        if not os.path.exists(self.save_folder):
            os.makedirs(self.save_folder)

        with open(self.save_folder + self.save_file_name, 'wb') as f:
            pickle.dump(self.rooms, f)
            pickle.dump(self.rooms_centers, f)
            pickle.dump(self.transitions, f)
            pickle.dump(self.graph, f)

    def load(self):
        with open(self.save_folder + self.save_file_name, 'rb') as f:
            self.rooms = pickle.load(f)
            self.rooms_centers = pickle.load(f)
            self.transitions = pickle.load(f)
            self.graph = pickle.load(f)


if __name__ == "__main__":
    map_ = cv2.imread("maps/map0.png")
    map_ = cv2.cvtColor(map_, cv2.COLOR_BGR2GRAY)

    planner = TrajectoryPlanner("maps")

    st = time()

    planner.examine_map(map_)
    path = planner.path(robot, new_robot)

    print(time() - st)

    color_map = cv2.cvtColor(map_, cv2.COLOR_GRAY2BGR)

    for pt in path:
        cv2.circle(color_map, pt, 10, 100, -1)

    # draw robot and new robot's place
    cv2.circle(color_map, robot, 5, (255, 0, 255), -1)
    cv2.circle(color_map, new_robot, 5, (0, 255, 255), -1)

    cv2.imshow('map', color_map)
    cv2.waitKey()
