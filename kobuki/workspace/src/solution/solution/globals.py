#!/usr/bin/python3
print("-> globals.py")

node = None
imgColRos = None
lidar = None
odom = None
odomA = None

rot = None
pos = None

rotA = None
posA = None

mousePos = None
targetPos = None
path = None
planner = None

qr_rot = None
qr_id = None
qr_center = None
qr_dist = None

pos_offset = (300, 300)
ang_offset = 0
click_num = 0

saw_qr = False

def dump():
    print("[GLOBALS]")
    print("rot\t\t:", rot)
    print("pos\t\t:", pos)
    print("mousePos\t:", mousePos)
    print("targetPos\t:", targetPos)
    print("pos_offset\t:", pos_offset)
    print("ang_offset\t:", ang_offset)
    print("click_num\t:", click_num)
    print("\n")
