#!/usr/bin/python3

from setuptools import setup

package_name = "solution"

setup(
    name = package_name,
    version  = "0.1.0",
    packages = [package_name],
    data_files = [
        ("share/ament_index/resource_index/packages", ["resource/" + package_name]),
        ("share/" + package_name, ["package.xml", "launch/drv_tb3.xml", "launch/drv_tb4.xml"]),
    ],
    install_requires = ["setuptools"],
    zip_safe = True,
    maintainer = "kit",
    maintainer_email = "kittnablude@gmail.com",
    description = "dummy",
    license = "you must accept that you hate ros and ros2",
    tests_require = ["pytest"],
    entry_points = {
        "console_scripts": [
            "main3 = solution.main3:main",
            "main4 = solution.main4:main",
            "angel_finder = solution.angel_finder"
        ],
    },
)
